---
title: Andrew (Shark) Landsverk
me: andrew-landsverk
draft: false

links:
  - display: GitHub
    url: https://github.com/Quantas
  - display: LinkedIn
    url: https://www.linkedin.com/in/andrew-landsverk/
---

Shark has been a professional software developer and system administrator for over 10 years. Before 
going into the field he received a bachelor's degree from the University of Wisconsin - Stout with a major 
in IT Management and a minor in Computer Science.

<!--more-->

Shark has had a passion for computers from an early age. Ever since he learned about the internet all those years ago, he hasn't been able to take his hands off of a computer. He has a constant desire to always be learning, improving, and sharing. 

In recent years Shark has become a mentor to his peers and others. He enjoys helping others learn which is why he decided to start SharkOps, which is a way to give back to Open Source by contributing projects and articles to help others succeed.
