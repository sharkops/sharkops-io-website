# SharkOps

SharkOps is home to open source projects, articles, blogs, and a community pertaining to a variety of topics including DevOps, Software Development, Home Lab and everything in between. 

At SharkOps we pride ourselves on being completely open, accessible, and inclusive to anyone who wishes to help, share knowledge, and mentor others. If you wish to make a contribution, or join the team, please seek us out on Gitter or submit a pull request to one of our projects to contribute.

## Communities


- {{< extlink href="https://gitter.im/sharkops-community/community" title="Gitter (Chat)">}}
- {{< extlink  href="https://gitlab.com/sharkops" title="GitLab" >}}
- {{< extlink  href="https://github.com/shark-ops" title="GitHub" >}}
